# Movie Search

## Task
Using IMDB's API at [omdbapi](http://www.omdbapi.com/), produce a simple two page site that allows: 

  -Searching for a movie
  - View details of that movie including its poster.


## Running this application:
  Clone this repo:  
  `git clone https://Bolaji1@bitbucket.org/Bolaji1/movie-search.git`
  
  Change directory into the cloned repo:  
  `cd movie-search`  
 
  Run npm install to install the necessary dependencies:  
  `npm install`
  
  Run the app using:  
    `npm run start`
    
 App should be running at:
  `http://localhost:8080/` (if not in use) 