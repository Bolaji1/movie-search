import webpack from 'webpack'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html',
  inject: 'body',
})

const PATHS = {
  app: path.join(__dirname, 'app'),
  build: path.join(__dirname, 'dist'),
}

const LAUNCH_COMMAND = process.env.npm_lifecycle_event

const isProduction = LAUNCH_COMMAND === 'production' // if this app is going to production

process.env.BABEL_ENV = LAUNCH_COMMAND

const productionPlugin = new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify('production'),
  },
})

const base = {
  entry: [
    PATHS.app,
  ],
  output: {
    path: PATHS.build,
    filename: 'index_bundle.js',
  },
  module: {
    loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
            { test: /\.(s*)css$/, loaders: ['style-loader', 'css-loader', 'sass-loader']},
            {test: /\.(png|jp(e*)g|svg)$/, loader: 'url-loader'},
    ],
  },
  resolve: {
    root: path.resolve('./app'),
  },
}
const developmentConfig = {
  devtool: 'cheap-module-inline-source-map',
  devServer: {
    hot: true,
    inline: true,
    progress: true,
    historyApiFallback: true,
    contentBase: __dirname + '/dist',
  },
  plugins: [HtmlWebpackPluginConfig, new webpack.HotModuleReplacementPlugin()],
}

// Again if this app is going to production
const productionConfig = {}

export default Object.assign({}, base,
    isProduction === true ? productionConfig : developmentConfig
)
