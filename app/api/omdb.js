import axios from 'axios'

const API_KEY = '19277fa8'

export function searchForMovie (search = 'pulp') {
  const params = `?s=${search}&apikey=${API_KEY}`
  const encodeURI = window.encodeURI(`http://www.omdbapi.com/${params}`)
  return axios.get(encodeURI)
}

export function findMovieByImdbID (id = 'tt0332732') {
  const params = `?i=${id}&apikey=${API_KEY}`
  const encodeURI = window.encodeURI(`http://www.omdbapi.com/${params}`)
  return axios.get(encodeURI)
}
