import React from 'react'
import Search from './Search'
import { Switch, Route } from 'react-router-dom'
import MovieDetail from './MovieDetail'

const App = () => (
    <div className='main'>
     <Switch>
      <Route exact={true} path='/' component={Search}/>
      <Route path='/:imdbID' component={MovieDetail}/>
     </Switch>
    </div>
)

export default App
