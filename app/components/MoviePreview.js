import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Image } from 'react-bootstrap'

const DEFAULT_IMAGE_SRC = 'http://via.placeholder.com/350x150'

class MoviePreview extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    const {
            Title,
            Poster,
            Year,
            imdbID,
        } = this.props
    const src = Poster === 'N/A' ? DEFAULT_IMAGE_SRC : Poster
    return (

            <li className='movie-item'>
                <NavLink to={`/${imdbID}`} className='movie-link'>
                    <Image
                        src={src}
                        alt={'image for movie'} thumbnail={true} bsClass='movie-image-preview'/>
                    <p>{Title}</p><span>{`(${Year})`}</span>
                </NavLink>
            </li>
        )
  }
}

export default MoviePreview
