import React from 'react'
import PropTypes from 'prop-types'
import Divider from '@material-ui/core/Divider'
import MoviePreview from './MoviePreview'
import { isEmpty } from 'lodash'

function MovieList (props) {
  const { movies = [] } = props

  return (
        <div className='movie-list'>
            {!isEmpty(movies) && <Divider />}
            <ul>
                {movies.map((movie, index) => (
                  <MoviePreview key={index} {...movie} />
                ))}
            </ul>
        </div>
    )
}

MovieList.propTypes = {
  movies: PropTypes.array.isRequired,
}

export default MovieList
