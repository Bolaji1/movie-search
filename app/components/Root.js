import React, { PropTypes } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import App from './App'
import AppBar from './Header'

const Root = () => (
         <Router >
            <div className='container'>
                <AppBar/>
                <Route path='/' component={App} />
                <div className='footer'></div>
            </div>
        </Router>
)

export default Root
