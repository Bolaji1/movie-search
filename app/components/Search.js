import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import MovieList from './MovieList'
import { searchForMovie } from '../api/omdb'
import ReactLoading from 'react-loading'

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    padding: theme.spacing.unit,
    width: 200,
  },
})

class Search extends Component {
	                    static propTypes ={

	}

	                    constructor (props) {
	                        super(props)

  this.state = {
    search: '',
    movies: [],
    isLoading: false,
  }
}

  _handleTextFieldChange = (e) => {
	                                              this.setState({ search: e.target.value })
  }

  _handleButtonClick = () => {
    this.setState({ isLoading: true })

    searchForMovie(this.state.search)
            .then(({ data, statusText }) => {
              const { Error, Search} = data

              if (statusText === 'OK') {
                if (!Error) {
                  this.setState({
                    movies: Search,
                    isLoading: false,
                    errorMessage: '',
                    search: '',
                  })
                } else {
                  this.setState({
                    errorMessage: Error,
                    movies: [],
                    isLoading: false,
                    search: '',
                  })
                }
              } else {
                this.setState({
                  errorMessage: 'Something went wrong!',
                  movies: [],
                  isLoading: false,
                  search: '',
                })
              }
            })
  }

	                    render () {
	                        const { classes } = this.props
	                        const { search, isLoading, movies, errorMessage } = this.state
	                        return (
            <div>
                <Paper className='movie-paper' elevation={4}>
                    <div className='movie-search'>
                        <TextField
                            id='search'
                            label={'Search for movie'}
                            type='search'
                            className={classes.textField}
                            margin='normal'
                            value={search || ''}
                            onChange={this._handleTextFieldChange}/>
                        <Button variant='raised' color='primary' className={classes.button} onClick={this._handleButtonClick} disabled={!search}>
                            Search
                        </Button>
                    </div>
                    {isLoading && <ReactLoading type='spinningBubbles' color='#32429c' height={50} width={50}/>}
                    {errorMessage && <p>{errorMessage}</p>}
                  <MovieList movies={movies}/>
                </Paper>

            </div>
        )
}
}

export default withStyles(styles)(Search)
