import React, { Component } from 'react'
import { Image } from 'react-bootstrap'
import { findMovieByImdbID } from '../api/omdb'
const DEFAULT_IMAGE_SRC = 'http://via.placeholder.com/350x150'

class MovieDetail extends Component {
	                    constructor (props) {
	                        super(props)

  this.state = {}
}
  componentDidMount= () => {
    const id = this.props.match.params.imdbID

    findMovieByImdbID(id)
            .then((result) => {
              if (result.statusText === 'OK') {
                this.setState({ ...result.data })
              }
            })
  }
	                    render () {
	                        const {
	        Title,
            Year,
            Plot,
            Director,
            Writer,
            Actors,
            imdbRating,
            Poster,
	    } = this.state
  const src = Poster === 'N/A' ? DEFAULT_IMAGE_SRC : Poster
		                    return (
			<div>
                <h1>{`${Title} (${Year})`}</h1>
                <div className='movie-detail'>
                    <Image
                        src={src}
                        alt={'image for movie'} thumbnail={true}/>
                    <div className='movie-metadata'>
                        <p>{Plot}</p>
                        <h5>Director: {Director}</h5>
                        <h5>Writer(s): {Writer}</h5>
                        <h5>Actors: {Actors}</h5>
                        <h5>Imdb Rating: {imdbRating}</h5>
                    </div>
                </div>
            </div>
			)
	}
}

export default MovieDetail
